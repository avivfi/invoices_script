<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angular-97105.firebaseio.com';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);


// update exiting user1 with "update" ///
$user1 = array(
"name" => "test from php1",
);

$firebase->update(DEFAULT_PATH.'/1/', $user1);



// replace exirting user2 with "set"  //

$user2 = array(
"name" => "test from php2",
);

$firebase->set(DEFAULT_PATH.'/2/', $user2);




// --- create new user with "push"   /////

$newUser = array(
"name" => "new user",
"email" => "newUser@newUser.com",
"posts" => [new1=>true, new2=>true]);
$newUserId = $firebase->push(DEFAULT_PATH.'', $newUser);
echo $newUserId;
echo '<br><br>';


/// --- delete user 4 with  "delete"  ///

$firebase->delete(DEFAULT_PATH.'/4/');


// ---  get user1 with "get" --- //

$user1 = $firebase->get(DEFAULT_PATH.'/1/');
echo $user1;

echo'<p> All Ok </p>';